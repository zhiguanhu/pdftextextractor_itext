﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        class BaseOptions
        {
            [Option("input-folder-path", Required = false, Default = "", HelpText = "")]
            public string InputFolder { get; set; }
            [Option("output-folder-path", Required = false, Default = "", HelpText = "")]
            public string OutputFolder { get; set; }
            [Option("input-bucket", Required = false, Default = "", HelpText = "")]
            public string InputBucket { get; set; }
            [Option("output-bucket", Required = false, Default = "", HelpText = "")]
            public string OutputBucket { get; set; }
            [Option('u', "object-store-url", Required = false, Default = "", HelpText = "")]
            public string ObjectStoreURL { get; set; }
            [Option('k', "access-key", Required = false, Default = "", HelpText = "")]
            public string AccessKey { get; set; }
            [Option('s', "access-secret", Required = false, Default = "", HelpText = "")]
            public string AccessSecret { get; set; }
            [Option("is-os-input", Required = false, Default = false, HelpText = "Use file from object store as input")]
            public bool IsObjectStoreInput { get; set; }
            [Option("is-os-output", Required = false, Default = false, HelpText = "Use file from object store as output")]
            public bool IsObjectStoreOutput { get; set; }
            [Option('p', "output-prefix", Required = false, Default = "Output", HelpText = "prefix for output files.")]
            public string OutputPrefix { get; set; }
            [Option('m', "max-page", Required = false, Default = -1, HelpText = "")]
            public int MaxPage { get; set; }
            [Option("with-extension", Required = false, Default = true, HelpText = "Sub output folder with or w/o extension")]
            public bool DirNameWithExtension { get; set; }
            [Option("overwrite", Required = false, Default = false, HelpText = "Overwrite existing results or not")]
            public bool Overwrite {get; set; }
            [Option('r', "recursive", Required = false, Default = false, HelpText = "Search folders recursively")]
            public bool Recursive { get; set; }
            [Option('e', "extension", Required = false, Default = ".pdf", HelpText = "Extension used to search for files.")]
            public string Extension { get; set; }
        }

        [Verb("extract_folder", HelpText = "Extract text from mutiple pdf files in folder/OS bucket.")]
        class TextExtractorFolderOptions: BaseOptions
        {
        }
        
        [Verb("extract_file", HelpText = "Extract text from single pdf file.")]
        class TextExtractorFileOptions: BaseOptions
        {
            [Option('i', "input-filename", Required = true, HelpText = "Input file name. Please include extension")]
            public string InputFileName { get; set; }
            [Option('o', "output-filename", Required = false, HelpText = "Output file name. Please don't include extension.")]
            public string OutputFileName { get; set; }
        }

        static IConfigurationRoot config;
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<TextExtractorFileOptions, TextExtractorFolderOptions>(args)
                   .WithParsed<TextExtractorFileOptions>(opts => DoTextExtractionForFile(opts))
                   .WithParsed<TextExtractorFolderOptions>(opts=> DoTextExtractionForFolder(opts));
        }

        private static int DoTextExtractionForFile(TextExtractorFileOptions opts)
        {
            FileStorageFactory factory = new FileStorageFactory();
            FileStorageFactoryArgs args = new FileStorageFactoryArgs(opts.IsObjectStoreInput, opts.IsObjectStoreOutput, opts.InputFolder, opts.OutputFolder, 
                opts.ObjectStoreURL, opts.AccessKey, opts.AccessSecret, opts.InputBucket, opts.OutputBucket, opts.OutputPrefix);

            var inputStorage = factory.GetStorage("input", args) as IInputStorage;
            var outputStorage = factory.GetStorage("output", args) as IOutputStorage;

            if(opts.Overwrite || !outputStorage.CheckIsCompleted(opts.InputFileName, opts.DirNameWithExtension))
            {
                var result = inputStorage.ExtractTextForFile(opts.InputFileName, opts.MaxPage);
                if (result.Count != 0)
                {
                    outputStorage.SaveResult(opts.InputFileName, opts.DirNameWithExtension, result);
                }
            }
            else
            {
                logger.Info("Extraction result for file {0} already exists", opts.InputFileName);
            }

            return 0;
        }

        private static int DoTextExtractionForFolder(TextExtractorFolderOptions opts)
        {
            FileStorageFactory factory = new FileStorageFactory();
            FileStorageFactoryArgs args = new FileStorageFactoryArgs(opts.IsObjectStoreInput, opts.IsObjectStoreOutput, opts.InputFolder, opts.OutputFolder, 
                opts.ObjectStoreURL, opts.AccessKey, opts.AccessSecret, opts.InputBucket, opts.OutputBucket, opts.OutputPrefix);

            var inputStorage = factory.GetStorage("input", args) as IInputStorage;
            var outputStorage = factory.GetStorage("output", args) as IOutputStorage;

            var withExtension = opts.DirNameWithExtension;
            var overwrite = opts.Overwrite;
            var maxPage = opts.MaxPage;

            var fileNames = inputStorage.GetFileNames(opts.Extension);
            foreach(var fileName in fileNames)
            {
                if(overwrite || !outputStorage.CheckIsCompleted(fileName, withExtension))
                {
                    var result = inputStorage.ExtractTextForFile(fileName, maxPage);
                    if (result.Count != 0)
                    {
                        outputStorage.SaveResult(fileName, withExtension, result);
                    }
                }
                else
                {
                    logger.Info("Extraction result for file {0} already exists", fileName);
                }
            }
            
            return 0;
        }
    }
}
