using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    /// <summary>
    /// File Storage Factory arguments.
    /// </summary>
    public class FileStorageFactoryArgs
    {
        public bool IsOSInput {get; set; }
        public bool IsOSOutput {get; set; }
        public string InputFolder {get; set; }
        public string OutputFolder {get; set; }
        public string OSUrl {get; set; }
        public string AccessKey {get; set; }
        public string AccessSecret {get; set; }
        public string InputBucket {get; set; }
        public string OutputBucket {get; set; }
        public string OutputPrefix {get; set; }
        
        public FileStorageFactoryArgs(bool isOSInput, bool isOSOutput, string iFolder, string oFolder, string url,
            string key, string secret, string iBucket, string oBucket, string prefix)
        {
            IsOSInput = isOSInput;
            IsOSOutput = isOSOutput;
            InputFolder = iFolder;
            OutputFolder = oFolder;
            OSUrl = url;
            AccessKey = key;
            AccessSecret = secret;
            InputBucket = iBucket;
            OutputBucket = oBucket;
            OutputPrefix = prefix;
        }
    }
    /// <summary>
    /// File storage factory.
    /// </summary>
    public class FileStorageFactory
    {
        /// <summary>
        /// Creates the storage.
        /// </summary>
        /// <returns>One of File storage subclasses.</returns>
        /// <param name="type">Storage Type, input or output.</param>
        /// <param name="args">File Storage Factory Arguments.</param>
        public FileStorage GetStorage(string type, FileStorageFactoryArgs args)
        {
            switch(type)
            {
                case "input":
                {
                    if (args.IsOSInput)
                    {
                        return new ObjectStorageInput(args.OSUrl, args.AccessKey, args.AccessSecret, args.InputBucket);
                    }
                    else
                    {
                        return new FileSystemInput(args.InputFolder);
                    }
                }
                case "output":
                {
                    if (args.IsOSOutput)
                    {
                        return new ObjectStorageOutput(args.OSUrl, args.AccessKey, args.AccessSecret, args.OutputBucket, args.OutputPrefix);
                    }
                    else
                    {
                        return new FileSystemOutput(args.OutputFolder);
                    }
                }
                default:
                {
                    throw new ArgumentNullException();
                }
            }
        }
    }
}