using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using PdfTextExtractorLib_iText;
using CommandLine;
using CommandLine.Text;
using System.Linq;
using NLog;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    public interface IInputStorage
    {
        List<string> GetFileNames(string fileExtension);
        Dictionary<int, List<ResultantText>> ExtractTextForFile(string fileName, int maxPage);
    }
}