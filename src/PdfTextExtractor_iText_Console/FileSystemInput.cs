using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    /// <summary>
    /// File system as input.
    /// </summary>
    public class FileSystemInput: FileStorage, IInputStorage
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Constructor
        public FileSystemInput (string path)
        {
            if(path == null)
            {
                logger.Error("No input folder path provided");
            }
            else
            {
                if (!Directory.Exists(path))
                {
                    logger.Error("{0} does not exist", path);
                }
                else
                {
                    StoragePath = path;
                    DirectoryInfo dir = new DirectoryInfo(path);
                    logger.Info("Get files from folder {0}", dir.FullName);
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets all the fileEntries from the directory.
        /// </summary>
        /// <returns>All the fileEntries.</returns>
        public override IList<string> GetFiles()
        {
            var fileEntries = Directory.GetFiles(StoragePath).ToList();
            return fileEntries;
        }
        /// <summary>
        /// Gets all the file names of files with certain extension.
        /// </summary>
        /// <returns>All the file names.</returns>
        /// <param name="fileExtension">File extension to search.</param>
        public List<string> GetFileNames(string fileExtension)
        {
            var fileEntries = Directory.GetFiles(StoragePath, "*" + fileExtension).ToList();
            var fileNames = new List<string>();
            foreach(var file in fileEntries)
            {
                fileNames.Add(Path.GetFileName(file));
            }
            return fileNames;
        }
        /// <summary>
        /// Extracts the texts for file using PdfTextExtractor_iText.
        /// </summary>
        /// <returns>The extracted ResultantTexts for each page.</returns>
        /// <param name="fileName">Filename.</param>
        /// <param name="maxPage">Maxpage.</param>
        public Dictionary<int, List<ResultantText>> ExtractTextForFile(string fileName, int maxPage)
        {
            var result = new Dictionary<int, List<ResultantText>>();
            string filePath = Path.Combine(StoragePath, fileName);

            if(!File.Exists(filePath))
            {
                logger.Error("File {0} does not exist", fileName);
            }
            else
            {
                logger.Info("Extracting file {0} from {1}", fileName, StoragePath);

                try
                {
                    var extractor = new iTextPdfTextExtractor(filePath);
                    result = extractor.ExtractTexts(maxPage);

                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }
            }
            return result;
        }
        #endregion
    }
}