using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    public class FileSystemOutput: FileStorage, IOutputStorage
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Constructor
        public FileSystemOutput(string path)
        {
            if(path == null)
            {
                logger.Error("No output folder path provided");
            }
            else
            {
                if (!Directory.Exists(path))
                {
                    logger.Error("{0} does not exist", path);
                }
                else
                {
                    StoragePath = path;
                    DirectoryInfo dir = new DirectoryInfo(path);
                    logger.Info("Store extraction results into folder {0}", dir.FullName);
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets all the subdirectories from the directory.
        /// </summary>
        /// <returns>All the subdirectories.</returns>
        public override IList<string> GetFiles()
        {
            var subDirs = Directory.GetDirectories(StoragePath).Select(Path.GetFileName).ToList();
            return subDirs;
        }
        /// <summary>
        /// Creates the sub folder path to store extraction results.
        /// </summary>
        /// <returns>The sub folder path.</returns>
        /// <param name="fileName">Filename.</param>
        /// <param name="withExtension">If set to <c>true</c> to include file extension in subfolder name.</param>
        /// <param name="marker">Marker.</param>
        public string CreateSubFolderPath(string fileName, bool withExtension, string marker = "")
        {
            var folderName = withExtension? fileName: Path.GetFileNameWithoutExtension(fileName);
            var subFolderPath = Path.Combine(StoragePath, marker + folderName);
            return subFolderPath;
        }
        /// <summary>
        /// Checks whether the extraction process of certain file is completed.
        /// </summary>
        /// <returns><c>true</c>, if the extraction results of certain file is completed, <c>false</c> otherwise.</returns>
        /// <param name="fileName">Filename.</param>
        /// <param name="withExtension">If set to <c>true</c> to include file extension in subfolder name.</param>
        public bool CheckIsCompleted(string fileName, bool withExtension)
        {
            var subFolderPath = this.CreateSubFolderPath(fileName, withExtension);
            if(Directory.Exists(subFolderPath))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Saves the extraction results of certain file.
        /// </summary>
        /// <param name="fileName">Filename.</param>
        /// <param name="withExtension">If set to <c>true</c> to include file extension in subfolder name.</param>
        /// <param name="result">ResultantTexts for each page.</param>
        public void SaveResult(string fileName, bool withExtension, 
            Dictionary<int, List<ResultantText>> result)
        {
            var oldSubFolderPath = this.CreateSubFolderPath(fileName, withExtension, "_");
            Directory.CreateDirectory(oldSubFolderPath);

            foreach(var pageResult in result)
            {
                var outputFilePath = Path.Combine(oldSubFolderPath, fileName + "." + (pageResult.Key - 1) + ".xml");
                iTextPdfTextExtractor.CreateXmlFile(pageResult.Value, outputFilePath);
            }

            // after complete, change subfolder name
            Thread.Sleep(1000);

            var newSubFolderPath = this.CreateSubFolderPath(fileName, withExtension);
            logger.Info("Save {0} extraction result to {1}", fileName, newSubFolderPath);
            Directory.Move(oldSubFolderPath, newSubFolderPath);
        }
        #endregion            
    }
}
