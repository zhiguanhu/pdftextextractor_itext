using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using PdfTextExtractorLib_iText;
using CommandLine;
using CommandLine.Text;
using System.Linq;
using NLog;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    /// <summary>
    /// Base class for different File storage classes.
    /// </summary>
    public abstract class FileStorage
    {
        public string StoragePath { get; set; }
        public abstract IList<string> GetFiles();
    }

    public enum FileStorageTypes
    {
        FileStystem,
        ObjectStroage
    }
}