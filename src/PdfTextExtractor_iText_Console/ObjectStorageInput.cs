using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    public class ObjectStorageInput: FileStorage, IInputStorage
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Properties
        public string AccessKey {get; set; }
        public string AccessSecret {get; set; }
        public string BucketName {get; set; }
        public S3Helper OSClientHelper {get; set; }
        #endregion

        #region Constructor
        public ObjectStorageInput(string url, string key, string secret, string bucketName)
        {
            StoragePath = url;
            AccessKey = key;
            AccessSecret = secret;
            BucketName = bucketName;
            OSClientHelper = new S3Helper(key, secret, url);

            logger.Info("Get file from bucket {0}/{1}", url, bucketName);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets all the object keys from the object storage.
        /// </summary>
        /// <returns>All the object keys.</returns>
        public override IList<string> GetFiles()
        {
            var listObjectKeys = OSClientHelper.ListObject(BucketName).GetAwaiter().GetResult();
            return listObjectKeys;
        }
        /// <summary>
        /// Gets all the file names of files with certain extension from the object storage.
        /// </summary>
        /// <returns>All the file names.</returns>
        /// <param name="fileExtension">File extension to search.</param>
        public List<string> GetFileNames(string fileExtension)
        {
            var listObjectKeys = this.GetFiles();
            var files = listObjectKeys.Where(key => key.EndsWith(fileExtension, StringComparison.CurrentCultureIgnoreCase)).ToList();
            return files;
        }
        /// <summary>
        /// Extracts the texts for file using PdfTextExtractor_iText.
        /// </summary>
        /// <returns>The extracted ResultantTexts for each page.</returns>
        /// <param name="fileName">Filename.</param>
        /// <param name="maxPage">Maxpage.</param>
        public Dictionary<int, List<ResultantText>> ExtractTextForFile(string fileName, int maxPage)
        {
            logger.Info("Extract file {0} from {1}/{2}", fileName, StoragePath, BucketName);

            using (var response = OSClientHelper.ReadObjectDataResponseAsync(BucketName, fileName).GetAwaiter().GetResult())
            {
                using (var stream = response.ResponseStream)
                {
                    var extractor= new iTextPdfTextExtractor(stream);
                    return extractor.ExtractTexts(maxPage);
                }
            }
        }
        #endregion
    }
}
