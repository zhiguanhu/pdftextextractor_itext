using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    public class ObjectStorageOutput: FileStorage, IOutputStorage
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Propterties
        public string AccessKey {get; set; }
        public string AccessSecret {get; set; }
        public string BucketName {get; set; }
        public string Prefix {get; set; }
        public S3Helper OSClientHelper {get; set; }
        public List<string> ListObjectKeys {get; set; }
        #endregion

        #region Constructor
        public ObjectStorageOutput (string url, string key, string secret, string bucketName, string prefix)
        {
            StoragePath = url;
            AccessKey = key;
            AccessSecret = secret;
            BucketName = bucketName;
            Prefix = prefix;
            OSClientHelper = new S3Helper(key, secret, url);

            logger.Info("Store extraction results into {0}/{1}/{2}", url, bucketName, prefix);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets all the object keys from the object storage.
        /// </summary>
        /// <returns>All the object keys.</returns>
        public override IList<string> GetFiles()
        {
            var listObjectKeys = OSClientHelper.ListObject(BucketName).GetAwaiter().GetResult();
            return listObjectKeys;
        }
        /// <summary>
        /// Gets all the object keys starts with certain prefix from the object storage.
        /// </summary>
        /// <returns>All the object keys.</returns>
        /// <param name="prefix">Prefix to search.</param>
        public List<string> GetFiles(string prefix)
        {
            //System.Console.WriteLine("Calling from OSOutput");
            var listObjectKeys = this.GetFiles();
            var fileKeys = listObjectKeys.Where(key => key.StartsWith(prefix)).ToList();
            return fileKeys;
        }
        /// <summary>
        /// Creates the sub prefix path to store extraction results.
        /// </summary>
        /// <returns>The sub prefix path.</returns>
        /// <param name="fileName">Filename.</param>
        /// <param name="withExtension">If set to <c>true</c> to include file extension in subprefix.</param>
        /// <param name="marker">Marker.</param>
        public string CreateSubFolderPath(string fileName, bool withExtension, string marker = "")
        {
            var subFolderName = withExtension? fileName: Path.GetFileNameWithoutExtension(fileName);
            var subPrefix = Path.Combine(Prefix, marker + subFolderName).Replace('\\', '/');
            return subPrefix;
        }
        /// <summary>
        /// Checks whether the extraction process of certain file is completed.
        /// </summary>
        /// <returns><c>true</c>, if the extraction results of certain file is completed, <c>false</c> otherwise.</returns>
        /// <param name="fileName">Filename.</param>
        /// <param name="withExtension">If set to <c>true</c> to include file extension in subprefix.</param>
        public bool CheckIsCompleted(string fileName, bool withExtension)
        {
            var subPrefix = this.CreateSubFolderPath(fileName, withExtension);
            if(this.GetFiles(subPrefix).Count >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Saves the extraction results of certain file.
        /// </summary>
        /// <param name="fileName">Filename.</param>
        /// <param name="withExtension">If set to <c>true</c> to include file extension in subprefix.</param>
        /// <param name="result">ResultantTexts for each page.</param>
        public void SaveResult(string fileName, bool withExtension, 
            Dictionary<int, List<ResultantText>> result)
        {
            var oldPrefix = this.CreateSubFolderPath(fileName, withExtension, "_");
            var newPrefix = this.CreateSubFolderPath(fileName, withExtension);
            var keyPairs = new List<(string oldKey, string newKey)>();

            foreach(var pageResult in result)
            {
                using (var stream = iTextPdfTextExtractor.CreateXmlStream(pageResult.Value))
                {
                    var oldKey = Path.Combine(oldPrefix, fileName + "." + (pageResult.Key - 1) + ".xml");
                    var newKey = Path.Combine(newPrefix, fileName + "." + (pageResult.Key - 1) + ".xml");
                    keyPairs.Add((oldKey, newKey));

                    OSClientHelper.WritingAnObjectStreamAsync(BucketName, oldKey, stream)
                    .GetAwaiter().GetResult();
                }
            }
            // after complete, change subprefix
            logger.Info("Save {0} extraction result into {1}", fileName, newPrefix);

            foreach(var pair in keyPairs)
            {
                OSClientHelper.CopyObjectAsync(BucketName, pair.oldKey, BucketName, pair.newKey).GetAwaiter().GetResult();
                OSClientHelper.DeleteObjectAsync(BucketName, pair.oldKey).GetAwaiter().GetResult();
            }
        }
        # endregion
    }
}