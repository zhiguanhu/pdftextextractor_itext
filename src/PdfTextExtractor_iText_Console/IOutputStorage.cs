using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Configuration;
using CommandLine;
using CommandLine.Text;
using NLog;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;

namespace PdfTextExtractor_iText_Console
{
    public interface IOutputStorage
    {
        string CreateSubFolderPath(string fileName, bool withExtension, string marker);
        bool CheckIsCompleted(string fileName, bool withExtension);
        void SaveResult(string fileName, bool withExtension, Dictionary<int, List<ResultantText>> result);

    }
}