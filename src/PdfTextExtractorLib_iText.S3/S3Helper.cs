﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Configuration;
using NLog;

namespace PdfTextExtractorLib_iText.S3
{
    public class S3Helper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        AmazonS3Config config = new AmazonS3Config
        {
            RegionEndpoint = RegionEndpoint.USEast1, // MUST set this before setting ServiceURL and it should match the `MINIO_REGION` enviroment variable.
            ServiceURL = "http://localhost:9000", // replace http://localhost:9000 with URL of your MinIO server
            ForcePathStyle = true // MUST be true to work correctly with MinIO server
        };
        AmazonS3Client amazonS3Client;
        public S3Helper(string serviceUrl)
        {
            config.ServiceURL = serviceUrl;
            amazonS3Client = new AmazonS3Client(config);
        }
        public S3Helper(string accessKey, string secretKey, string serviceUrl)
        {
            config.ServiceURL = serviceUrl;
            AccessKey = accessKey;
            SecretKey = secretKey;
            amazonS3Client = new AmazonS3Client(AccessKey, SecretKey, config);
        }
        /// <summary>
        /// Read object into a memory stream by copying the origin stream from response.
        /// </summary>
        /// <param name="bucketName">Bucket name</param>
        /// <param name="keyName">Key name for the file.</param>
        /// <returns></returns>
        public async Task<Stream> ReadObjectDataStreamFromCopyAsync(string bucketName, string keyName)
        {
            Stream outStream = new MemoryStream();
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName
                };
                using (GetObjectResponse response = await amazonS3Client.GetObjectAsync(request))
                {
                    response.ResponseStream.CopyTo(outStream);
                    outStream.Position = 0;
                }
                return outStream;
            }
            catch (AmazonS3Exception e)
            {
                logger.Error("Error encountered ***. Message:'{0}' when reading an object", e.Message);
                return null;
            }
            catch (Exception e)
            {
                logger.Error("Unknown encountered on server. Message:'{0}' when reading an object", e.Message);
                return null;
            }
        }
        /// <summary>
        /// Get response of read object data. The request is not closed, so make sure to dispose response after use it.
        /// </summary>
        /// <param name="bucketName">Bucket Name</param>
        /// <param name="keyName">Key Name</param>
        /// <returns></returns>
        public async Task<GetObjectResponse> ReadObjectDataResponseAsync(string bucketName, string keyName)
        {
            try
            {
                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName
                };
                GetObjectResponse response = await amazonS3Client.GetObjectAsync(request);
                return response;
            }
            catch (AmazonS3Exception e)
            {
                logger.Error("Error encountered ***. Message:'{0}' when reading an object", e.Message);
                return null;
            }
            catch (Exception e)
            {
                logger.Error("Unknown encountered on server. Message:'{0}' when reading an object", e.Message);
                return null;
            }
        }
        /// <summary>
        /// List all the objects.
        /// </summary>
        /// <param name="bucketName">Bucket Name.</param>
        /// <returns></returns>
        public async Task<List<String>> ListObject(string bucketName)
        {
            // uncomment the following line if you like to troubleshoot communication with S3 storage and implement private void OnAmazonS3Exception(object sender, Amazon.Runtime.ExceptionEventArgs e)
            // amazonS3Client.ExceptionEvent += OnAmazonS3Exception;

            //TODO: Need to make sure that we can still get files that's inside a "folder(prefix)".
            var listObjectsResponse = await amazonS3Client.ListObjectsAsync(bucketName);
            var objects = new List<string>();

            foreach (var obj in listObjectsResponse.S3Objects)
            {
                logger.Info("Find object {0}", obj.Key);
                objects.Add(obj.Key);
            }
            return objects;
        }
        /// <summary>
        /// List all the objects.
        /// </summary>
        /// <returns></returns>
        public async Task ListObject()
        {
            // uncomment the following line if you like to troubleshoot communication with S3 storage and implement private void OnAmazonS3Exception(object sender, Amazon.Runtime.ExceptionEventArgs e)
            // amazonS3Client.ExceptionEvent += OnAmazonS3Exception;

            var listBucketResponse = await amazonS3Client.ListBucketsAsync();

            foreach (var bucket in listBucketResponse.Buckets)
            {
                Console.Out.WriteLine("bucket '" + bucket.BucketName + "' created at " + bucket.CreationDate);
            }
            if (listBucketResponse.Buckets.Count > 0)
            {
                var bucketName = listBucketResponse.Buckets[0].BucketName;

                var listObjectsResponse = await amazonS3Client.ListObjectsAsync(bucketName);

                foreach (var obj in listObjectsResponse.S3Objects)
                {
                    Console.Out.WriteLine(
                        "key = '" + obj.Key +
                        "' | size = " + obj.Size +
                        " | tags = '" + obj.ETag +
                        "' | modified = " + obj.LastModified +
                        "' | ETag = " + obj.ETag +
                        "' | Owner = " + obj.Owner +
                        "' | StorageClass = " + obj.StorageClass);

                }
            }
        }
        public async Task WritingAnObjectStreamAsync(
            string bucketName, string key, Stream inputStream, string contentType = "application/pdf")
        {
            try
            {
                // Put the object-set ContentType and add metadata.
                var putRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = key,
                    ContentType = contentType,
                    InputStream = inputStream
                };
                // Add metadata
                //putRequest.Metadata.Add("x-amz-meta-title", "Test stream Pdf");

                PutObjectResponse response = await amazonS3Client.PutObjectAsync(putRequest);
            }
            catch (AmazonS3Exception e)
            {
                logger.Error(
                        "Error encountered ***. Message:'{0}' when writing an object"
                        , e.Message);
            }
            catch (Exception e)
            {
                logger.Error(
                    "Unknown encountered on server. Message:'{0}' when writing an object"
                    , e.Message);
            }
        }
        public async Task CopyObjectAsync(string sourceBucket, string oldKey, string destinationBucket, string newKey)
        {
            try
            {
                var copyRequest = new CopyObjectRequest
                {
                    SourceBucket = sourceBucket,
                    SourceKey = oldKey,
                    DestinationBucket = destinationBucket,
                    DestinationKey = newKey
                };

                CopyObjectResponse response = await amazonS3Client.CopyObjectAsync(copyRequest);
            }
            catch (AmazonS3Exception e)
            {
                logger.Error("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
        }

        public async Task DeleteObjectAsync(string bucketName, string key)
        {
            try
            {
                DeleteObjectResponse response = await amazonS3Client.DeleteObjectAsync(bucketName, key);
            }
            catch (AmazonS3Exception e)
            {
                logger.Error("Error encountered ***. Message:'{0}' when writing an object", e.Message);
            }
        }

        private static (string url, string access, string secret) GetS3ConfigurationSettings(IConfigurationRoot config)
        {
            //static IConfigurationRoot config;
            // Init config for s3 settings.
            //var builder = new ConfigurationBuilder()
            //       .AddJsonFile(@"s3_config.json");
            //config = builder.Build();

            var url = config.GetSection("service_url").Value;
            var access = config.GetSection("access").Value;
            var secret = config.GetSection("secret").Value;
            return (url, access, secret);
        }
    }
}
