using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using PdfTextExtractorLib_iText;

namespace PdfTextExtractorApi.Models
{
    public class TextResult: ResultantText
    {
        [Key]
        [Column("file_id")]
        public string fileId { get; set; }
        [Key]
        [Column("text_id")]
        public string textId { get; set; }

        public TextResult()
        {
        }

        public TextResult(ResultantText txt, string fId, string tId)
        {
            this.Text = txt.Text;
            this.Top = txt.Top;
            this.Left = txt.Left;
            this.Width = txt.Width;
            this.Height = txt.Height;
            this.Font = txt.Font;
            this.FontSize = txt.FontSize;
            this.IsRotated = txt.IsRotated;
            this.RotationDegree = txt.RotationDegree;
            this.TextRotation = txt.TextRotation;
            this.PageNo = txt.PageNo;
            this.PageHeight = txt.PageHeight;
            this.PageWidth = txt.PageWidth;
            this.PageRotation = txt.PageRotation;
            this.IsVisible = txt.IsVisible;
            
            fileId = fId;
            textId = tId;
        }
    }
}
