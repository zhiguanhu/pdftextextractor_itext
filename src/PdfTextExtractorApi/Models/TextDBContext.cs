using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PdfTextExtractorApi.Models
{
    public class TextDbContext: DbContext
    {
        public TextDbContext(DbContextOptions<TextDbContext> options) : base(options)
        {

        }
        public virtual DbSet<TextResult> TextResult { get; set; }
        public virtual DbSet<Files> Files { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TextResult>()
                .HasKey(c => new { c.fileId, c.textId });
            modelBuilder.Entity<Files>()
                .HasKey(c => new { c.fileId });
        }
    }
}
