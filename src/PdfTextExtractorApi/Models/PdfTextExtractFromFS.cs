using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PdfTextExtractorApi.Models
{
    /// <summary>
    /// Extract text from pdf file that located at filesystem.
    /// </summary>
    public class PdfTextExtractFromFS:IValidatableObject
    {
        [Required]
        public string SrcPath {get; set; }
        public List<int> PageRange {get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if(!System.IO.File.Exists(SrcPath))
            {
                yield return new ValidationResult($"{SrcPath} doesn't exist!.", new []{"SrcPath"});
            }

            if(PageRange != null && PageRange.Contains(0))
            {
                yield return new ValidationResult("PageRange can not contain 0");
            }
        }
    }
}