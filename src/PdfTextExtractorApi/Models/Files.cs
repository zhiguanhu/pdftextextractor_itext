using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PdfTextExtractorApi.Models
{
    public class Files
    {
        [Key]
        [Column("file_id")]
        public string fileId { get; set; }
        [Column("file_name")]
        public string fileName { get; set; }
        [Column("source_path")]
        public string srcPath { get; set; }
        [Column("is_processed")]
        public string isProcessed {get; set; }
        [Column("process_time")]
        public DateTime processTime { get; set; }
    }
}