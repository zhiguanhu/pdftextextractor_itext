using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PdfTextExtractorApi.Models
{
    /// <summary>
    /// Extract text from pdf file that located at S3/MinIO.
    /// </summary>
    public class PdfTextExtractFromS3:IValidatableObject
    {
        [Required]
        public string ServiceUrl {get; set; }
        [Required]
        public string BucketName {get; set; }
        [Required]
        public string FileName {get; set; }
        public string AccessKey {get; set; }
        public string SecretKey {get; set; }
        public int MaxNumber {get; set; } = -1;
        public List<int> PageRange {get; set; }

        /// <summary>
        /// If this value = "url", the api will write result into a file stored at the object storage.
        /// if the value = "file", it will just return the result inside response body.
        /// </summary>
        /// <value></value>
        public string ReturnOption {get; set; } = "file";
        public string InputPrefix {get; set; } = "Input";
        public string OutputPrefix {get; set; } = "Output";

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            if(!ReturnOption.Equals("file") && ! ReturnOption.Equals("url"))
            {
                yield return new ValidationResult($"Return option has to be either 'file' or 'url'. Your input is {ReturnOption}.", new []{"ReturnOption"});
            }

        }
    }
}