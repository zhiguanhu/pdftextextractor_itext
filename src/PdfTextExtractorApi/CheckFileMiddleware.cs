using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PdfTextExtractorApi.Controllers;
using PdfTextExtractorApi.Models;

namespace PdfTextExtractorApi
{
    public sealed class CheckFileMiddleware
    {
        private readonly RequestDelegate _next;
        public CheckFileMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }
        public async Task InvokeAsync(HttpContext context)
        {
            var req = context.Request;
            req.EnableRewind();
            var reader = new StreamReader(req.Body);
            var body = reader.ReadToEnd();
            req.Body.Position = 0;

            PdfTextExtractFromFS file = JsonConvert.DeserializeObject<PdfTextExtractFromFS>(body);
            var srcPath = file.SrcPath;
            if (!File.Exists(srcPath))
            {
                context.Response.StatusCode = 404;
                await context.Response.WriteAsync("File not found");
                return;
            }
            else
            {
                await _next(context);
            }
        } 
    }

    public static class CheckFileMiddlewareExtensions
    {
        public static IApplicationBuilder UseCheckFileMiddleware(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<CheckFileMiddleware>();
        }
    }

}
