using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;
using PdfTextExtractorApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace PdfTextExtractorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly ILogger<ExtractorController> _logger;
        private readonly TextDbContext _context;
        public FileController(ILogger<ExtractorController> logger, TextDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        // GET: api/file
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Files>>> GetFiles()
        {
            return await _context.Files.ToListAsync();
        }

        // GET: api/extractor/files/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Files>> GetFiles(string id)
        {
            var file = await _context.Files.FindAsync(id);
            if (file == null)
            {
                return StatusCode(404, "This file does not exists.");
            }
            return file;
        }
    }
}
