using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PdfTextExtractorLib_iText;
using PdfTextExtractorLib_iText.S3;
using PdfTextExtractorApi.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace PdfTextExtractorApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExtractorController : ControllerBase
    {
        private readonly ILogger<ExtractorController> _logger;
        private readonly TextDbContext _context;
        public ExtractorController(ILogger<ExtractorController> logger, TextDbContext context)
        {
            _logger = logger;
            _context = context;
        }
        // Testing endpoint
        // Get: api/extractor
        [HttpGet]
        public IEnumerable<string> ExtractTextstring()
        {
            _logger.LogTrace("Got a test request.");
            return new string[] { "value1", "value2", "value3" };
        }

        // POST: api/extractor/filesystem/textstring
        [HttpPost("filesystem/textstring")]
        public IActionResult ExtractTextstring([FromBody]PdfTextExtractFromFS file)
        {
            var srcPath = file.SrcPath;
            var pageRange = file.PageRange;
            var extractor = new iTextPdfTextExtractor(srcPath);

            _logger.LogTrace("Extracting {file}", srcPath);

            if (pageRange == null)
            {
                var textstrings = extractor.ExtractTextStrings();
                return Ok(textstrings);                
            }
            else
            {
                var textstrings = extractor.ExtractTextStrings(pageRange);
                return Ok(textstrings);    
            }
        }

        // POST: api/extractor/filesystem/text
        [HttpPost("filesystem/text")]
        public IActionResult ExtractText([FromBody]PdfTextExtractFromFS file)
        {
            var srcPath = file.SrcPath;
            var pageRange = file.PageRange;            
            var extractor = new iTextPdfTextExtractor(srcPath);
            
            var texts = new Dictionary<int, List<ResultantText>>();
            var results = new List<ResultantText>();

            _logger.LogTrace("Extracting {file}", srcPath);

            if (pageRange == null)
            {
                texts = extractor.ExtractTexts();
            }
            else
            {
                texts = extractor.ExtractTexts(pageRange);
            }

            foreach (var text in texts)
            {
                results.AddRange(text.Value);
            }
            return Ok(results);
        }

        // POST: api/extractor/text
        [HttpPost("text")]
        public async Task<ActionResult<Files>> PostTexts([FromBody]PdfTextExtractFromFS inputFile)
        {
            // create new file
            Files newFile = new Files();
            newFile.fileId = Guid.NewGuid().ToString();
            newFile.fileName = Path.GetFileName(inputFile.SrcPath);
            newFile.srcPath = inputFile.SrcPath;
            newFile.isProcessed = "true";
            newFile.processTime = DateTime.Now;

            _context.Files.Add(newFile);
            await _context.SaveChangesAsync();

            // create new texts
            _logger.LogTrace("Extracting {file}", newFile.srcPath);
            var pageRange = inputFile.PageRange;            
            var extractor = new iTextPdfTextExtractor(newFile.srcPath);

            // var results = extractor.ExtractTexts(pageRange);
            var results = new Dictionary<int, List<ResultantText>>();
            if (pageRange == null)
            {
                results = extractor.ExtractTexts();
            }
            else
            {
                results = extractor.ExtractTexts(pageRange);
            }

            foreach (var pageResult in results)
            {
                foreach (var text in pageResult.Value)
                {
                    var fileId = newFile.fileId;
                    var textId = Guid.NewGuid().ToString();
                    TextResult newText = new TextResult(text, fileId, textId);

                    _context.TextResult.Add(newText);
                    await _context.SaveChangesAsync();
                }
            }
            return CreatedAtAction("GetFiles", new {id = newFile.fileId}, newFile);
        }

        // POST: api/extractor/objectstorage/text
        [HttpPost("objectstorage/text")]
        public IActionResult ExtractTextsFromStream([FromBody]PdfTextExtractFromS3 file)
        {
            var url = file.ServiceUrl;
            var accessKey = file.AccessKey;
            var secretKey = file.SecretKey;
            var bucketName = file.BucketName;
            var fileName = file.FileName;
            var maxNumber = file.MaxNumber;
            var returnOption = file.ReturnOption;
            var outputPrefix = file.OutputPrefix;

            _logger.LogTrace("Extracting {url}, {bucket}, {filename}", url, bucketName, fileName);

            var s3Helper = new S3Helper(accessKey, secretKey, url);
            using (var response = s3Helper.ReadObjectDataResponseAsync(bucketName, fileName).GetAwaiter().GetResult())
            {
                try 
                {
                    var stream = response.ResponseStream;
                    var extractor = new iTextPdfTextExtractor(stream);
                    var texts =  extractor.ExtractTexts(maxNumber);

                    if(returnOption == "file")
                    {
                        var results = new List<ResultantText>();
                        foreach (var text in texts)
                        {
                            results.AddRange(text.Value);
                        }
                        return Ok(results);
                    }
                    if(returnOption == "url")
                    {
                        var results = new List<string>();
                        foreach (var singlePageResult in texts)
                        {
                            var outputPath = Path.Combine(outputPrefix, fileName, fileName + "." + (singlePageResult.Key - 1) + ".xml").Replace('\\', '/');
                            results.Add(outputPath);
                            using (var outStream = iTextPdfTextExtractor.CreateXmlStream(singlePageResult.Value))
                            {
                                s3Helper.WritingAnObjectStreamAsync(bucketName, outputPath, outStream)
                                .GetAwaiter().GetResult();
                            }
                        }
                        return Ok(results);
                    }
                    else
                    {
                        return BadRequest("Not valid output option");
                    }

                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }

        // POST: api/extractor/objectstorage/textstring
        [HttpPost("objectstorage/textstring")]
        public IActionResult ExtractTextstringsFromStream([FromBody]PdfTextExtractFromS3 file)
        {
            var url = file.ServiceUrl;
            var accessKey = file.AccessKey;
            var secretKey = file.SecretKey;
            var bucketName = file.BucketName;
            var key = file.FileName;
            var maxNumber = file.MaxNumber;

            var s3Helper = new S3Helper(accessKey, secretKey, url);
            using (var response = s3Helper.ReadObjectDataResponseAsync(bucketName, key).GetAwaiter().GetResult())
            {
                try 
                {
                    var stream = response.ResponseStream;
                    var extractor = new iTextPdfTextExtractor(stream);
                    var texts =  extractor.ExtractTextStrings(maxNumber);

                    return Ok(texts);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
        }
    }
}
