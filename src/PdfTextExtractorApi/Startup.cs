﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using PdfTextExtractorApi.Models;

namespace PdfTextExtractorApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddMvc().AddXmlSerializerFormatters();
            services.AddSwaggerDocument(config => 
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "PdfTextExtractor API";
                    // document.Info.Description = "A simple ASP.NET Core web API";
                    // document.Info.TermsOfService = "None";
                };
            });
            services.AddDbContext<TextDbContext>(options =>
                options.UseSqlite("Filename=./ExtractorTest.db")
                // options.UseInMemoryDatabase(databaseName: "test")
                // options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"))
                );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }
            
            app.UseMiddleware<CustomExceptionMiddleware>();
            // Register the Swagger generator and the Swagger UI middleware
            app.UseOpenApi();
            app.UseSwaggerUi3();
            
            app.UseMvc();
            SeedData.Initialize(app.ApplicationServices);
        }

        // dbInitializer
        public static class SeedData
        {
            public static void Initialize(IServiceProvider serviceProvider)
            {
                using (var serviceScope = serviceProvider.CreateScope())
                {
                    var context = serviceScope.ServiceProvider.GetService<TextDbContext>();
                    //increase timeout period for testing purpose
                    context.Database.SetCommandTimeout(90);
                    // auto migration
                    context.Database.EnsureCreated();
                }
            }
        }
    }
}
