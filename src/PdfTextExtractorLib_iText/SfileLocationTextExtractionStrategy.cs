using System;
using System.Collections.Generic;
using iText.Kernel.Geom;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Data;
using iText.Kernel.Pdf.Canvas.Parser.Listener;

namespace PdfTextExtractorLib_iText
{
    /// <summary>
    /// Customized TextChunk including defaultTextChunk and other desired information for further analysis.
    /// </summary>
    public class SfileTextChunk
    {
        public TextChunk DefaultTextChunk;
        public double TextChunkWidth;
        public double TextChunkHeight;
        public double TextChunkTop;
        public double TextChunkLeft;
        public string TextFont;
        public float TextFontSize;
        public bool IsVisible;
        public Vector OrientationVector;

        public SfileTextChunk(
            TextChunk tc, double width, double height, double top, double left, 
            string font, float size, bool visiblity, Vector vector)
        {
            this.DefaultTextChunk = tc;
            this.TextChunkWidth = width;
            this.TextChunkHeight = height;
            this.TextChunkTop = top;
            this.TextChunkLeft = left;
            this.TextFont = font;
            this.TextFontSize = size;
            this.IsVisible = visiblity;
            this.OrientationVector = vector;
        } 
    }
    /// <summary>
    /// Customized location text extraction strategy.
    /// </summary>
    public class SfileLocationTextExtractionStrategy : LocationTextExtractionStrategy
    {
        private TextRenderInfo lastTextRenderInfo;
        private bool useActualText = false;
        /// <summary>a summary of all found text</summary>

        private readonly IList<TextChunk> locationalResult = new List<TextChunk>();

        private readonly IList<SfileTextChunk> completedResult = new List<SfileTextChunk>();

        private readonly LocationTextExtractionStrategy.ITextChunkLocationStrategy tclStrat;
        private CanvasTag FindLastTagWithActualText(IList<CanvasTag> canvasTagHierarchy)
        {
            CanvasTag lastActualText = null;
            foreach (CanvasTag tag in canvasTagHierarchy)
            {
                if (tag.GetActualText() != null)
                {
                    lastActualText = tag;
                    break;
                }
            }
            return lastActualText;
        }
        public SfileLocationTextExtractionStrategy()
            : this(new _ITextChunkLocationStrategy_85())
        {
        }

        private sealed class _ITextChunkLocationStrategy_85 : LocationTextExtractionStrategy.ITextChunkLocationStrategy
        {
            public _ITextChunkLocationStrategy_85()
            {
            }

            public ITextChunkLocation CreateLocation(TextRenderInfo renderInfo, LineSegment baseline)
            {
                return new SfileTextChunkLocationImp(baseline.GetStartPoint(), baseline.GetEndPoint(), renderInfo.GetSingleSpaceWidth());
            }

            public Vector CreateVector(TextRenderInfo renderInfo, LineSegment baseline)
            {
                return new SfileTextChunkLocationImp(baseline.GetStartPoint(), baseline.GetEndPoint(), 
                renderInfo.GetSingleSpaceWidth()).OrientationVector();
            }

        }

        /// <summary>
        /// Creates a new text extraction renderer, with a custom strategy for
        /// creating new TextChunkLocation objects based on the input of the
        /// TextRenderInfo.
        /// </summary>
        /// <param name="strat">the custom strategy</param>
        public SfileLocationTextExtractionStrategy(LocationTextExtractionStrategy.ITextChunkLocationStrategy strat)
        {
            tclStrat = strat;
        }
        /// <summary>
        /// Changes the behavior of text extraction so that if the parameter is set to
        /// <see langword="true"/>
        /// ,
        /// /ActualText marked content property will be used instead of raw decoded bytes.
        /// Beware: the logic is not stable yet.
        /// </summary>
        /// <param name="useActualText">true to use /ActualText, false otherwise</param>
        /// <returns>this object</returns>
        public override iText.Kernel.Pdf.Canvas.Parser.Listener.LocationTextExtractionStrategy SetUseActualText(bool
             useActualText)
        {
            this.useActualText = useActualText;
            return this;
        }
        public override void EventOccurred(IEventData data, EventType type)
        {
            if (type.Equals(EventType.RENDER_TEXT))
            {
                TextRenderInfo renderInfo = (TextRenderInfo)data;
                LineSegment baseLine = renderInfo.GetBaseline();
                LineSegment ascentLine = renderInfo.GetAscentLine();
                LineSegment leftLine = new LineSegment(baseLine.GetStartPoint(), ascentLine.GetStartPoint());

                var width = baseLine.GetLength();
                var height = leftLine.GetLength();
                var top = ascentLine.GetStartPoint().Get(1);
                var left = ascentLine.GetStartPoint().Get(0);
                
                var textFont = renderInfo.GetFont().GetFontProgram().ToString();
                var textFontSize = renderInfo.GetFontSize();  // not always acurate

                var isVisible = true;
                if (renderInfo.GetTextRenderMode() == 3)
                {
                    isVisible = false;
                }

                if (renderInfo.GetRise() != 0)
                {
                    // remove the rise from the baseline - we do this because the text from a super/subscript render operations should probably be considered as part of the baseline of the text the super/sub is relative to
                    Matrix riseOffsetTransform = new Matrix(0, -renderInfo.GetRise());
                    baseLine = baseLine.TransformBy(riseOffsetTransform);
                }
                if (useActualText)
                {
                    CanvasTag lastTagWithActualText = lastTextRenderInfo != null ? FindLastTagWithActualText(lastTextRenderInfo
                        .GetCanvasTagHierarchy()) : null;
                    if (lastTagWithActualText != null && lastTagWithActualText == FindLastTagWithActualText(renderInfo.GetCanvasTagHierarchy
                        ()))
                    {
                        // Merge two text pieces, assume they will be in the same line
                        TextChunk lastTextChunk = locationalResult[locationalResult.Count - 1];
                        Vector mergedStart = new Vector(Math.Min(lastTextChunk.GetLocation().GetStartLocation().Get(0), baseLine.GetStartPoint
                            ().Get(0)), Math.Min(lastTextChunk.GetLocation().GetStartLocation().Get(1), baseLine.GetStartPoint().Get
                            (1)), Math.Min(lastTextChunk.GetLocation().GetStartLocation().Get(2), baseLine.GetStartPoint().Get(2)));
                        Vector mergedEnd = new Vector(Math.Max(lastTextChunk.GetLocation().GetEndLocation().Get(0), baseLine.GetEndPoint
                            ().Get(0)), Math.Max(lastTextChunk.GetLocation().GetEndLocation().Get(1), baseLine.GetEndPoint().Get(1)
                            ), Math.Max(lastTextChunk.GetLocation().GetEndLocation().Get(2), baseLine.GetEndPoint().Get(2)));
                        TextChunk merged = new TextChunk(lastTextChunk.GetText(), tclStrat.CreateLocation(renderInfo, new LineSegment
                            (mergedStart, mergedEnd)));
                        locationalResult[locationalResult.Count - 1] = merged;
                    }
                    else
                    {
                        String actualText = renderInfo.GetActualText();
                        TextChunk tc = new TextChunk(actualText != null ? actualText : renderInfo.GetText(), tclStrat.CreateLocation
                            (renderInfo, baseLine));
                        locationalResult.Add(tc);
                    }
                }
                else
                {
                    TextChunk tc = new TextChunk(renderInfo.GetText(), tclStrat.CreateLocation(renderInfo, baseLine));
                    Vector oVector = ((_ITextChunkLocationStrategy_85)tclStrat).CreateVector(renderInfo, baseLine);
                    locationalResult.Add(tc);

                    // Construct customized TextChunk
                    SfileTextChunk SfileTC = new SfileTextChunk(
                        tc, width, height, top, left, textFont, textFontSize, isVisible, oVector);
                    completedResult.Add(SfileTC);

                }
                lastTextRenderInfo = renderInfo;
            }
        }

        public IList<TextChunk> GetResultantTextChunks()
        {
            return locationalResult;
        }

        public IList<SfileTextChunk> GetSfileTextChunks()
        {
            return completedResult;
        }
    }
}