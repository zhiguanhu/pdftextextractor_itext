using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;

namespace PdfTextExtractorLib_iText
{
    /// <summary> Resultant text including desired information.</summary>
    [XmlType(TypeName = "text")]
    public class ResultantText: IResultantText
    {
        [XmlAttribute(AttributeName = "top")]
        public double Top {get; set; }
        [XmlAttribute(AttributeName = "left")]
        public double Left {get; set; }
        [XmlAttribute(AttributeName = "width")]
        public double Width {get; set; }
        [XmlAttribute(AttributeName = "height")]
        public double Height {get; set; }
        [XmlAttribute(AttributeName = "font")]
        public string Font {get; set; }
        [XmlAttribute]
        public float FontSize {get; set; }
        [XmlText]
        public string Text {get; set; }
        [XmlAttribute]
        public bool IsRotated {get; set; }
        [XmlAttribute]
        public double RotationDegree {get; set; }
        [XmlAttribute]
        public double TextRotation {get; set; }
        [XmlAttribute]
        public int PageNo {get; set; }
        [XmlAttribute]
        public float PageWidth {get; set; }
        [XmlAttribute]
        public float PageHeight {get; set; }
        [XmlAttribute]
        public int PageRotation {get; set; }
        [XmlAttribute]
        public bool IsVisible {get; set; }

        // Add a default constructor for xml serialization
        public ResultantText ()
        {
        }
        public ResultantText (SfileTextChunk SfileTC, PageInfo pageInfo)
        {
            Text = SfileTC.DefaultTextChunk.GetText();
            Width = SfileTC.TextChunkWidth;
            Height = SfileTC.TextChunkHeight;
            Top = SfileTC.TextChunkTop;
            Left = SfileTC.TextChunkLeft;
            Font = SfileTC.TextFont;
            FontSize = SfileTC.TextFontSize;
            IsVisible = SfileTC.IsVisible;

            PageNo = pageInfo.PageNo;
            PageWidth = pageInfo.PageWidth;
            PageHeight = pageInfo.PageHeight;
            PageRotation = pageInfo.PageRotation;
        }

        public override string ToString() 
        {
            return "text: " + Text + " |width: " + Width + " |height: " + Height + " | font: " + Font;
        }
        public void ExportXmlFile(List<ResultantText> texts, string outPath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ResultantText>));
            TextWriter writer = new StreamWriter(outPath);
            serializer.Serialize(writer, texts);
            writer.Close();
        }
        
    }
}