using System;

namespace PdfTextExtractorLib_iText
{
    public interface IResultantText
    {
        double Top {get; }
        double Left {get; }
        double Width {get; }
        double Height {get; }
        string Font {get; }
        float FontSize {get; }
        string Text{get; }
        bool IsRotated {get; }
        double RotationDegree {get; }
        double TextRotation {get; }
        int PageNo {get; }
        float PageWidth {get; }
        float PageHeight {get; }
        int PageRotation {get; }
        bool IsVisible {get; }
        

    }
}