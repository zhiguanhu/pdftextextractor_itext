using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Data;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using NLog;

namespace PdfTextExtractorLib_iText
{
    /// <summary> Page information used to construct ResultantText. </summary>
    public class PageInfo
    {
        public int PageNo {get; set; }
        public float PageWidth {get; set; }
        public float PageHeight {get; set; }
        public int PageRotation {get; set; }
        public PageInfo(PdfPage page)
        {
            PageWidth = page.GetPageSize().GetWidth();
            PageHeight = page.GetPageSize().GetHeight();
            PageRotation = page.GetRotation();
        }
    }
    /// <summary> Pdf text extractor using iText 7 library. </summary>
    public class iTextPdfTextExtractor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        PdfDocument pdfDoc;
        /// <summary>
        /// Take file as input.
        /// </summary>
        /// <param name="srcPath">Pdf file path.</param>
        public iTextPdfTextExtractor(string srcPath)
        {
            pdfDoc = new PdfDocument(new PdfReader(srcPath));
        }
        /// <summary>
        /// Take stream as input
        /// </summary>
        /// <param name="pdfStream">Pdf file stream.</param>
        public iTextPdfTextExtractor(Stream pdfStream)
        {
            pdfDoc = new PdfDocument(new PdfReader(pdfStream));
        }
        /// <summary>
        /// Extracts text strings from certain pages of pdf file.
        /// </summary>
        /// <returns>List of text strings.</returns>
        /// <param name="pageRange">List of page numbers to be extracted from.</param>
        public IList<string> ExtractTextStrings(List<int> pageRange)
        {
            var result = new List<string>();

            foreach (var pageNo in pageRange)
            {
                var textLocations = pdfDoc.GetPage(pageNo).ExtractTextLocation();
            
                foreach(var text in textLocations)
                {
                    var textStr = text.GetText();
                    result.Add(textStr);
                }
            }
            return result;
        }
        /// <summary>
        /// Extracts text strings from all pages of pdf file.
        /// </summary>
        /// <returns>List of text strings.</returns>
        /// <param name="srcPath">Path of source pdf file.</param>
        public IList<string> ExtractTextStrings(int maxPage = -1)
        {
            var pageNums = pdfDoc.GetNumberOfPages();
            if(maxPage > -1)
            {
                pageNums = Math.Min(maxPage, pageNums); 
            }
            var pageRange = new List<int>();
            for (var i = 1; i<= pageNums; i++)
            {
                pageRange.Add(i);
            }
            return ExtractTextStrings(pageRange);
        }
        /// <summary>
        /// Extracts texts with customized information from certain pages of pdf file.
        /// </summary>
        /// <returns>Dictionary including page number and list of texts in pair.</returns>
        /// <param name="srcPath">Path of source pdf file.</param>
        /// <param name="pageRange">List of page numbers to be extracted from.</param>
        public Dictionary<int, List<ResultantText>> ExtractTexts(List<int> pageRange)
        {
            var result = new Dictionary<int, List<ResultantText>>();

            foreach (var pageNo in pageRange)
            {
                try
                {
                    PdfPage page = pdfDoc.GetPage(pageNo);
                    var texts = ExtractTextsFromPage(page, pageNo);
                    result.Add(pageNo, texts);
                }catch (Exception e)
                {
                    logger.Error("Extraction failed on page {0}: {1}", pageNo, e.ToString());
                }
            }
            return result;
        }
        /// <summary>
        /// Extracts texts with customized information from all pages of pdf file.
        /// </summary>
        /// <returns>Dictionary including page number and list of texts in pair.</returns>
        /// <param name="srcPath">Path of source pdf file.</param>
        public Dictionary<int, List<ResultantText>> ExtractTexts(int maxPage = -1)
        {
            var pageNums = pdfDoc.GetNumberOfPages();
            if (maxPage > -1)
            {
                pageNums = Math.Min(maxPage, pageNums);
            }
            var pageRange = new List<int>();
            for (var i = 1; i<= pageNums; i++)
            {
                pageRange.Add(i);
            }
            return ExtractTexts(pageRange);
        }
        /// <summary>
        /// Extracts the texts with courstomized information from single page.
        /// </summary>
        /// <returns>List of texts.</returns>
        /// <param name="page"> Single pdf Page.</param>
        /// <param name="pageNo">Page number.</param>
        public List<ResultantText> ExtractTextsFromPage(PdfPage page, int pageNo)
        {
            var texts = new List<ResultantText>();
            var strategy = new SfileLocationTextExtractionStrategy();
            PdfTextExtractor.GetTextFromPage(page, strategy);                
            var textChunks = strategy.GetSfileTextChunks();
            var pageInfo = new PageInfo(page);
            pageInfo.PageNo = pageNo;
            
            foreach(var textChunk in textChunks)
            {
                ResultantText text = new ResultantText(textChunk, pageInfo);

                var oritentionVector = textChunk.OrientationVector.Normalize();
                if (oritentionVector.Get(0) != 1 || oritentionVector.Get(1) != 0 || oritentionVector.Get(2) != 0) 
                {
                    text.TextRotation = CalculateTextRotation(oritentionVector);
                }
                
                text.RotationDegree = (text.PageRotation + text.TextRotation) % 360;
                if (text.RotationDegree != 0)
                { text.IsRotated = true; }
                
                texts.Add(text);
            }
            return texts;
        }
        /// <summary>
        /// Calculates the text rotation.
        /// </summary>
        /// <returns>The text rotation degree.</returns>
        /// <param name="oVector">Oritention vector of text chunk.</param>
        private double CalculateTextRotation(Vector oVector)
        {
            var horizonVector = new Vector(1, 0, 0);
            var angle = Math.Acos(oVector.Dot(horizonVector)) * 180 / Math.PI;
            if (oVector.Get(1) > 0)
            { angle = 360 - angle; }
            return angle;
        }
        /// <summary>
        /// Creates the xml file for extracted texts.
        /// </summary>
        /// <param name="texts">List of texts with customized information.</param>
        /// <param name="outPath">File path to write.</param>
        public static void CreateXmlFile(List<ResultantText> texts, string outPath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ResultantText>));
            TextWriter writer = new StreamWriter(outPath);
            serializer.Serialize(writer, texts);
            writer.Close();
        }
        
        public static Stream CreateXmlStream(List<ResultantText> texts)
        {
            var outputStream = new MemoryStream();

            XmlSerializer serializer = new XmlSerializer(typeof(List<ResultantText>));
            TextWriter writer = new StreamWriter(outputStream);

            serializer.Serialize(writer, texts);
            writer.Flush();
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }
    }

    public static class ReaderExtensions
    {
        //Work based on https://stackoverflow.com/questions/48597948/text-extraction-from-a-pdf-using-itext7-how-to-improve-its-performance
        public static string[] ExtractText(this PdfPage page)
        {
            var pageRect = page.GetMediaBox();
            return page.ExtractText(pageRect);
        }
        public static string[] ExtractText(this PdfPage page, params Rectangle[] rects)
        {
            var textEventListener = new LocationTextExtractionStrategy();
            PdfTextExtractor.GetTextFromPage(page, textEventListener);
            string[] result = new string[rects.Length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = textEventListener.GetResultantText(rects[i]);
            }
            return result;
        }
        public static IList<TextChunk> ExtractTextLocation(this PdfPage page)
        {
            var pageRect = page.GetMediaBox();
            return page.ExtractTextLocation(pageRect);
        }
        public static IList<TextChunk> ExtractTextLocation(this PdfPage page, params Rectangle[] rects)
        {
            var textEventListener = new LocationTextExtractionStrategy();
            PdfTextExtractor.GetTextFromPage(page, textEventListener);
            List<TextChunk> result = new List<TextChunk>();
            for (int i = 0; i < rects.Length; i++)
            {
                result.AddRange(textEventListener.GetResultantTextRects(rects[i]));
                // System.Console.WriteLine($"The result is {result}");
            }
            return result;
        }
        public static String GetResultantText(this LocationTextExtractionStrategy strategy, Rectangle rect)
        {
            IList<TextChunk> locationalResult = (IList<TextChunk>)locationalResultField.GetValue(strategy);
            List<TextChunk> nonMatching = new List<TextChunk>();
            foreach (TextChunk chunk in locationalResult)
            {
                ITextChunkLocation location = chunk.GetLocation();
                Vector start = location.GetStartLocation();
                Vector end = location.GetEndLocation();
                if (!rect.IntersectsLine(start.Get(Vector.I1), start.Get(Vector.I2), end.Get(Vector.I1), end.Get(Vector.I2)))
                {
                    nonMatching.Add(chunk);
                }
            }
            nonMatching.ForEach(c => locationalResult.Remove(c));
            try
            {
                return strategy.GetResultantText();
            }
            finally
            {
                nonMatching.ForEach(c => locationalResult.Add(c));
            }
        }
        public static IList<TextChunk> GetResultantTextRects(
            this LocationTextExtractionStrategy strategy, Rectangle rect)
        {
            IList<TextChunk> locationalResult =
                (IList<TextChunk>)locationalResultField.GetValue(strategy);
            List<TextChunk> nonMatching = new List<TextChunk>();
            List<TextChunk> matchedTextChunks = new List<TextChunk>();
            foreach (TextChunk chunk in locationalResult)
            {
                ITextChunkLocation location = chunk.GetLocation();
                Vector start = location.GetStartLocation();
                Vector end = location.GetEndLocation();
                if (rect.IntersectsLine(
                    start.Get(Vector.I1), start.Get(Vector.I2),
                    end.Get(Vector.I1), end.Get(Vector.I2)))
                {
                    matchedTextChunks.Add(chunk);
                }
            }
            return matchedTextChunks;
        }
        static FieldInfo locationalResultField =
            typeof(LocationTextExtractionStrategy)
            .GetField("locationalResult", BindingFlags.NonPublic | BindingFlags.Instance);
    }

}