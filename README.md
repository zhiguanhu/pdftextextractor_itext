# This project is to extract text from PDF files

[![Build status](https://ci.appveyor.com/api/projects/status/8tt76a13cegw92b9?svg=true)](https://ci.appveyor.com/project/zhiguanhu/pdftextextractor-itext)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=zhiguan-hu_pdftextextractor_itext&metric=alert_status)](https://sonarcloud.io/dashboard?id=zhiguan-hu_pdftextextractor_itext)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=zhiguan-hu_pdftextextractor_itext&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=zhiguan-hu_pdftextextractor_itext)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=zhiguan-hu_pdftextextractor_itext&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=zhiguan-hu_pdftextextractor_itext)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=zhiguan-hu_pdftextextractor_itext&metric=sqale_index)](https://sonarcloud.io/dashboard?id=zhiguan-hu_pdftextextractor_itext)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=zhiguan-hu_pdftextextractor_itext&metric=ncloc)](https://sonarcloud.io/dashboard?id=zhiguan-hu_pdftextextractor_itext)


### Prefix for new branches:

1. Bug fix: bugfix/

2. New feature: feature/

3. Urgent fix: hotfix/

4. Release: release/


### PdfTextExtractor Docker Instruction

This instruction demonstrates how to run PdfTextExtractor in docker as a REST API to extract text strings and corresponding information of location and rotation, from Pdf files, either from local file system or object storage.

install docker
build image
run
test with swagger

1. Install Docker on local machines.

2. Run `docker build -t IMAGE_NAME -f Dockerfile .` to build a docker image and create a local repository named _IMAGENAME_ that points to this image. Run `docker images` to verify the successful build of the images.

3. Two ways to run the project.

- Retain the created container for mutiple runs. 
    Use `docker create IMAGE_NAME --CONTAINER_NAME ` to create a container based on the image with a specific name.
    Use `docker start` and `docker stop` to start and stop the container.
    Use `docker rm` to delete the container previously created.

- Single run.
    Use `docker run -it --rm -p HOST_PORT_NO:80 IMAGE_NAME` to create and run the container using current terminal, and remove the container when it finishes.

4. Experience the PdfTextExtractor as a REST API and interact with Swagger UI.

- Go to localhost/PORT_NO/swagger to view all public action methods.
    The web UI looks like this:
    ![PdfTextExtractor Web UI](/doc/images/PdfTextExtractorWebUI.png)

- Each public action mehtod in extractor controller can be tested from the web UI. Click a method name to explore and provide necessary parameters to try it out.
    For example: 
    use Extractor/stream/text to extract text from pdf file in object storage
    ![Extractor Example](/doc/images/ActionMethodExample.png)

    choose file as ReturnOption to display the extraction result in response
    ![Result Example](doc/images/ExtractFromOSResult.png)


