FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY PdfTextExtractor_iText.sln .
COPY src/ .
RUN dotnet restore PdfTextExtractorLib_iText.S3/PdfTextExtractorLib_iText.S3.csproj
RUN dotnet restore PdfTextExtractorLib_iText/PdfTextExtractorLib_iText.csproj
RUN dotnet restore PdfTextExtractorApi/PdfTextExtractorApi.csproj
RUN dotnet restore PdfTextExtractor_iText_Console/PdfTextExtractor_iText_Console.csproj
RUN dotnet build PdfTextExtractorApi/PdfTextExtractorApi.csproj -c Release -o /app

FROM build AS publish
WORKDIR /src
RUN dotnet publish PdfTextExtractorApi/PdfTextExtractorApi.csproj -c Release -o /app

FROM base AS final
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "PdfTextExtractorApi.dll"]