using System;
using Xunit;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Serialization;
using PdfTextExtractorLib_iText;

namespace PdfTextExtractorTest_iText
{
    public static class TestHelper
    {
        public static string BaseOutputFolder = @"../../../../test_output";
        public static string BaseInputFolder = @"../../../../test_data";
        public static List<string> TestFiles = new List<string>{"ocg", "text_orientations", "page_orientations"};
        /// <summary>
        /// Creates the output path for each xml file.
        /// </summary>
        /// <returns>The output directory and file path.</returns>
        /// <param name="fileName">File name to be extracted.</param>
        /// <param name="pageNo">Page number.</param>
        public static (string outputDir, string outputFile) CreateOutputPath (string fileName, int pageNo)
        {
            var currentDir = Directory.GetCurrentDirectory();
            var outputDir = Path.Combine(currentDir, BaseOutputFolder, fileName);
            var outputFile = Path.Combine(outputDir, fileName + "_"+ pageNo + ".xml");

            if (!Directory.Exists(outputDir))
                Directory.CreateDirectory(outputDir);
            return (outputDir, outputFile);
        }
        /// <summary>
        /// Creates the xml file to present results.
        /// </summary>
        /// <param name="result">List of texts extracted from single page.</param>
        /// <param name="path">Path of file to write.</param>
        public static void CreateXml(List<ResultantText> result, string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<ResultantText>));
            TextWriter writer = new StreamWriter(path);
            serializer.Serialize(writer, result);
            writer.Close();
        }
        /// <summary>
        /// Extracts the texts from pdf file and export xml file for each page.
        /// </summary>
        /// <param name="fileName">File name to be extracted.</param>
        public static List<string> ExtractTextFromStream(string fileName)
        {
            var currentDir = Directory.GetCurrentDirectory();
            var srcFile = Path.Combine(currentDir, BaseInputFolder, fileName + ".pdf");
            var outputFiles = new List<String>();

            if (!File.Exists(srcFile))
            {
                System.Diagnostics.Debug.WriteLine("File does not exist: {0}", srcFile);
                return outputFiles;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine($"Processing {srcFile}");
            }
            var pdfStream = new FileStream(srcFile, FileMode.Open);
            var extractor = new iTextPdfTextExtractor(pdfStream);
            var results = extractor.ExtractTexts();

            foreach (var result in results)
            {
                var outputPath = CreateOutputPath(fileName, result.Key);
                outputFiles.Add(outputPath.outputFile);
                CreateXml(result.Value, outputPath.outputFile);
            }
            return outputFiles;
        }
        /// <summary>
        /// Extracts the texts from pdf file and export xml file for each page.
        /// </summary>
        /// <param name="fileName">File name to be extracted.</param>
        public static List<String> ExtractText(string fileName)
        {
            var currentDir = Directory.GetCurrentDirectory();
            var srcFile = Path.Combine(currentDir, BaseInputFolder, fileName + ".pdf");
            var outputFiles = new List<String>();

            if (!File.Exists(srcFile))
            {
                System.Diagnostics.Debug.WriteLine("File does not exist: {0}", srcFile);
                return outputFiles; 
            }else{
                System.Diagnostics.Debug.WriteLine($"Processing {srcFile}");
            }
            var extractor = new iTextPdfTextExtractor(srcFile);
            var results = extractor.ExtractTexts();

            foreach (var result in results)
            {
                var outputPath = CreateOutputPath(fileName, result.Key);
                outputFiles.Add(outputPath.outputFile);
                CreateXml(result.Value, outputPath.outputFile);
            }
            return outputFiles;
        }
        public static void CleanTestFiles(string outputDir, List<string> outputFiles)
        {
            if (Directory.Exists(outputDir))
            {
                // Delete files first in case the directory exists before our test
                // and accidentaly delete other files.
                outputFiles.ForEach(x => File.Delete(x));
                Directory.Delete(outputDir);
            }
        }
    }
    public class ExtractorTest
    {
        /// <summary>
        /// Should create xml files as output.
        /// </summary>
        /// <param name="fileName">Pdf file to be extracted.</param>
        /// <param name="expected">If set to <c>true</c> expected.</param>
        [Theory]
        [InlineData("text_rotation_90", true)]
        [InlineData("wide_text", true)]
        [InlineData("corrupted_file", false)]
        public void CanCreateXml(string fileName, bool expected)
        {
            var outputDir = TestHelper.CreateOutputPath(fileName, 1).outputDir;
            var outputfile = TestHelper.CreateOutputPath(fileName, 1).outputFile;
            var outputFiles = TestHelper.ExtractText(fileName);

            Assert.Equal(expected, File.Exists(outputfile));
            if (Directory.Exists(outputDir))
            {
                // Delete files first in case the directory exists before our test
                // and accidentaly delete other files.
                outputFiles.ForEach(x => File.Delete(x));
                Directory.Delete(outputDir);
            }
        }
        [Theory]
        [InlineData("text_rotation_90_stream", true)]
        [InlineData("wide_text_stream", true)]
        [InlineData("corrupted_file_stream", false)]
        public void CanCreateXmlFromFileStream(string fileName, bool expected)
        {
            var outputDir = TestHelper.CreateOutputPath(fileName, 1).outputDir;
            var outputfile = TestHelper.CreateOutputPath(fileName, 1).outputFile;
            var outputFiles = TestHelper.ExtractTextFromStream(fileName);

            Assert.Equal(expected, File.Exists(outputfile));

            TestHelper.CleanTestFiles(outputDir, outputFiles);
        }
    }
    public class ExtractResultTest
    {
        /// <summary>
        /// Should extract correct number of texts from certain page.
        /// </summary>
        [Fact]
        public void TestTextCount()
        {
            var fileName = TestHelper.TestFiles[0];
            var outputDir = TestHelper.CreateOutputPath(fileName, 1).outputDir;
            var outputFile = TestHelper.CreateOutputPath(fileName, 1).outputFile;

            var outputFiles = TestHelper.ExtractText(fileName);
            // var count1 = XDocument.Load(outputFile).Descendants("text").Count();
            var count = XDocument.Load(outputFile).XPathSelectElements(".//text").Count();
            Assert.Equal(10, count);

            TestHelper.CleanTestFiles(outputDir, outputFiles);
        }

        /// <summary>
        /// Should extract correct word from certain page.
        /// </summary>
        [Fact]
        public void TestExtractCorrectWord()
        {
            var fileName = TestHelper.TestFiles[0];
            var outputDir = TestHelper.CreateOutputPath(fileName, 1).outputDir;
            var outputFile = TestHelper.CreateOutputPath(fileName, 1).outputFile;

            var outputFiles = TestHelper.ExtractText(fileName);

            XDocument Doc = XDocument.Load(outputFile);
            var extractedElements = Doc.Descendants("text");
            List<string> extractedTexts = new List<string>();
            foreach (XElement el in extractedElements)
            {
                extractedTexts.Add(el.Value);
            }
            Assert.Contains("option 3", extractedTexts);

            TestHelper.CleanTestFiles(outputDir, outputFiles);
        }
        /// <summary>
        /// Should extract correct text rotation information.
        /// </summary>
        [Fact]
        public void TestExtractCorrectDegree()
        {
            var fileName = TestHelper.TestFiles[2];
            var outputDir = TestHelper.CreateOutputPath(fileName, 2).outputDir;
            var outputFile = TestHelper.CreateOutputPath(fileName, 2).outputFile;

            var outputFiles = TestHelper.ExtractText(fileName);
            var extractedText = XDocument.Load(outputFile).Descendants("text")
            .Where(x => (string)x.Value == "A simple page in landscape orientation").FirstOrDefault();

            Assert.Equal("90", extractedText.Attribute("RotationDegree").Value);
            TestHelper.CleanTestFiles(outputDir, outputFiles);
        }
    }
    
}
